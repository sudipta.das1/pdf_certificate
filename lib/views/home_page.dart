import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/material.dart';
import 'package:pdf_certificate/controller/pdf_controller.dart';
import 'package:pdf_certificate/widgets/custom_button.dart';
import 'package:pdf_certificate/widgets/custom_text_fill.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController nameController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  Future<void> createPDF(String name) async {
    final PdfDocument document = PdfDocument();
    final page = document.pages.add();
    page.graphics.drawImage(PdfBitmap(await getImages('img.png')),
        const Rect.fromLTWH(0, 120, 510, 400));

    page.graphics.drawString(
      name,
      PdfStandardFont(PdfFontFamily.helvetica, 16),
      bounds: const Rect.fromLTWH(200, 290, 400, 100),
      brush: PdfBrushes.black,
    );

    List<int> bytes = await document.save();
    document.dispose();
    PDFcontroller().saveAndLaunchFile(bytes, 'cirtificate.pdf');
  }

  Future<Uint8List> getImages(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(160, 24, 255, 255),
        title: Text(
          'Certificate'.toUpperCase(),
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w400,
            letterSpacing: 2,
          ),
        ),
        centerTitle: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      backgroundColor: Colors.blueGrey[400],
      body: Center(
          child: Form(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomTextFormFild(
              controller: nameController,
              prefixIcon: const Icon(
                Icons.person,
                color: Colors.tealAccent,
              ),
              hintText: 'Enter Your Good Name',
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please Enter a Name';
                } else {
                  return null;
                }
              },
            ),
            const SizedBox(height: 50),
            CustomButton(
                height: 40,
                width: 150,
                icon: const Icon(
                  Icons.done,
                  color: Colors.white,
                ),
                lable: 'SUBMIT',
                onpressed: () {
                  formKey.currentState!.validate()
                      ? createPDF(nameController.text)
                      : null;
                })
          ],
        ),
      )),
    );
  }
}
