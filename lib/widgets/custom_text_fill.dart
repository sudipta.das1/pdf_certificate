import 'package:flutter/material.dart';

class CustomTextFormFild extends StatefulWidget {
  final TextEditingController controller;
  final Icon prefixIcon;
  final String hintText;
  final Function(String)? validator;

  final bool alutovalidation;
  // final TextInputType keybordtype;
  final bool readOnly;

  const CustomTextFormFild({
    super.key,
    required this.controller,
    required this.prefixIcon,
    required this.hintText,
    required this.validator,
    this.alutovalidation = false,
    // this.keybordtype = TextInputType.text,
    this.readOnly = false,
  });

  @override
  State<CustomTextFormFild> createState() => _CustomTextFormFildState();
}

class _CustomTextFormFildState extends State<CustomTextFormFild> {
  bool isobscure = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
          readOnly: widget.readOnly,
          // keyboardType: widget.keybordtype,
          obscureText: isobscure,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w300, fontSize: 14),
          cursorColor: Colors.white,
          controller: widget.controller,
          autovalidateMode: widget.alutovalidation
              ? AutovalidateMode.onUserInteraction
              : AutovalidateMode.disabled,
          decoration: InputDecoration(
            prefixIcon: widget.prefixIcon,
            hintText: widget.hintText,
            border: const OutlineInputBorder(),
            labelText: widget.hintText,
            hintStyle: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w300,
              fontSize: 16,
            ),
            labelStyle: const TextStyle(
              color: Colors.tealAccent,
              fontSize: 18,
              fontWeight: FontWeight.w300,
            ),
            contentPadding:
                const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.tealAccent),
              borderRadius: BorderRadius.circular(10),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.tealAccent),
              borderRadius: BorderRadius.circular(10),
            ),
            errorBorder: OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color.fromARGB(255, 225, 0, 0)),
              borderRadius: BorderRadius.circular(10),
            ),
            focusedErrorBorder: OutlineInputBorder(
                borderSide:
                    const BorderSide(color: Color.fromARGB(255, 225, 0, 0)),
                borderRadius: BorderRadius.circular(10)),
            errorStyle: const TextStyle(
              color: Color.fromARGB(255, 255, 69, 69),
              fontSize: 12.9,
              fontFamily: 'Roboto-Italic',
            ),
          ),
          validator: (value) =>
              widget.validator == null ? null : widget.validator!(value ?? '')),
    );
  }
}
