import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  Icon icon;
  String lable;
  double height;
  double width;
  final Function onpressed;
  CustomButton({
    super.key,
    required this.icon,
    required this.lable,
    this.height = 50,
    this.width = 200,
    required this.onpressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      alignment: Alignment.center,
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.blueGrey.shade500,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.tealAccent),
        boxShadow: const [
          BoxShadow(
              color: Color.fromARGB(33, 100, 255, 219),
              blurRadius: 10.0,
              spreadRadius: 2.0,
              offset: Offset(
                7.0,
                8.0,
              ))
        ],
      ),
      child: TextButton.icon(
          onPressed: () {
            onpressed();
          },
          icon: icon,
          label: Text(
            lable,
            style: const TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w400,
                letterSpacing: 2),
          )),
    );
  }
}
