import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:open_file/open_file.dart' as open_file;

class PDFcontroller {
  Future<void> saveAndLaunchFile(List<int> bytes, String fileName) async {
    final path = (await getExternalStorageDirectory())!.path;
    final file = File('$path/$fileName');
    await file.writeAsBytes(bytes, flush: true);
    open_file.OpenFile.open('$path/$fileName');
  }
}
